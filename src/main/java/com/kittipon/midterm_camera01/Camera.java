/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.midterm_camera01;

/**
 *
 * @author kitti
 */
public class Camera {

    String camera = "CAAMEERAA";

    public int max_Memory(){
        return 100;
    }
//    protected int max_Memory = 100;
    protected int now_Memory = max_Memory();
    int num;
    public void said(){
        System.out.println("Now memory is " + max_Memory());
    }
    public void Press(String dosomething, boolean turn) {
//        System.out.println("check one shutter");
        if (dosomething.equals("S") && turn == true) {
            Shutter();
        } else if (dosomething.equals("D") && turn == true) {
            Delete();
        } else {
            System.out.println("Error!!!");
        }

    }

//*****************VVVV
    public void Press(String dosomething, int number, boolean turn) {
//        System.out.println("check burst shutter");
        this.num = number;
//        System.out.println(num);
        if (dosomething.equals("B") && turn == true) {
            if (num > 10) {
                System.out.println("Error!!!");
            } else if (num <= 10) {
                Shutter(num);
            }

        } else if (dosomething.equals("D") && turn == true) {
            Delete(num);
        } else {
            System.out.println("Error!!!");
        }

    }

    public void ShowName() {
        System.out.println("This camera is " + camera + ".");
    }

    public void Shutter() {
        if (now_Memory <= 0) {
            System.out.println("You dont have enough memory now!!");
            System.out.println("Please delete some photo or change your memory");
        } else if (now_Memory <= (max_Memory() / 100) * 20) {
            ShowCanShutter();
            System.out.println("*** Your memory is almost full!!! ***");
            now_Memory--;
            ShowMemory();
        } else {
            ShowCanShutter();
            now_Memory--;
            ShowMemory();
        }

    }

    public void Shutter(int num) {
        if (now_Memory <= 0) {
            System.out.println("You dont have enough memory now!!");
            System.out.println("Please delete some photo or change your memory");
        } else if (now_Memory <= (max_Memory() / 100) * 20) {
            for (int i = 0; i < num; i++) {
                now_Memory--;
            }
            ShowCanShutter(num);
            System.out.println("*** Your memory is almost full!!! ***");
            ShowMemory();
        } else {
            for (int i = 0; i < num; i++) {
                now_Memory--;
            }
            ShowCanShutter(num);
            ShowMemory();
        }

    }

    public void Delete() {
        if (now_Memory < max_Memory()) {
            System.out.print("You delete 1 of photo and ");
            now_Memory++;
            ShowMemory();
        } else if (now_Memory >= max_Memory()) {
            System.out.print("No have no picture to delete.");
            ShowMemory();
        }

    }

    public void Delete(int num) {
        if (now_Memory < max_Memory()) {
            if ((now_Memory + num) > max_Memory()) {
                num = now_Memory + num - max_Memory();
            }
            System.out.print("You delete " + num + "  of photo and ");
            for (int i = 0; i < num; i++) {
                now_Memory++;
            }
            ShowMemory();
        } else if (now_Memory >= max_Memory()) {
            System.out.print("No have no picture to delete.");
            ShowMemory();
        }

    }

    protected void ShowCanShutter() {
        System.out.println("You got that photo");
    }

    protected void ShowCanShutter(int countNum) {
        System.out.println("You got " + countNum + " photo");
    }

    protected void ShowMemory() {
        if (now_Memory > max_Memory()) {
            now_Memory = max_Memory();
        }
        System.out.println("now you have " + now_Memory + " photo left.");
    }

}
